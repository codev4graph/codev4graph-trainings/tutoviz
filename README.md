# Codev4Graph Tuto for visualizing networks with Vue

[DEMO HERE](https://codev4graph.pages.mia.inra.fr/codev4graph-trainings/tutoviz/)

You can find useful resources here:
https://codev4graph.pages.mia.inra.fr/codev4graph-site/documentations/tech


## Objective of the tutorial

The objective is to handle network entities (nodes, links, layout, styles) with Vue Components.
Vue will display the network, i.e. the nodes and the links. Other libraries, such as d3, could be used to compute node positions or other things impossible to do in vue.

### Vue concepts covered in the tutorial

- Typescript in Vue
- Reactivity
- composable
- computed, watchers
- v-for, v-if
- Vue hooks (e.g. onMounted)
- Events
- Dynamic components
- Packaging
- Tests

### Structure of the tutorial

Each step of the tutorial corresponds to a specific branch.

Try the exercises. If you fail, look at the solution in the code or go to the next branch.

#### 1. Installation

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/1-Installation)

Start with a template that contains everything to build your first Vue components!


#### 2. Create a Node component

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/2-NodeComponent)

- Use Vue props
- Create types in typescript
- Learn how to encapsulate svg elements into Vue Components.

**Go further**: add props to add style to your nodes !

#### 3. Drag & Drop

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/3-dragAndDropWithD3/)

- Use d3 for drag & drop
- Emit an event to change the x, y positions in the network data

**Go further**: use native html methods to do the same thing : [Alternative solution](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/nativeDragAndDrop/) (be careful, not sure that it works well !)

#### 4. Set up a pan zoom

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/4-PanZoom/)

- Use vue-svg-pan-zoom to pan & zoom your network

#### 5. Create the link component

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/5-AddLink)

- Create the Link typescript type
- Create the Link vue component
- Create a computed variable to compute the path between two nodes

#### 6. Create the network Component

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/6-AddNetworkComponent/)

- Create the Network typescript type
- Combine the Link and Node components in one component

#### 7. Create a Random Network

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/7-RandomNetwork)

- Test the performance of the network by creating a Random Network
- Use a composable to launch the method

#### 7.5 Write tests

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/7.5-Tests)

#### 8. Arrange the node positions with a force layout

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/8-ForceLayout)

- Create a composable to compute the node positions with the d3 Force Layout
- export a method to fit the zoom once the network is created

#### 8.5. Create a node contextual menu

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/8.10-ContextualMenu)

- Get the click event on nodes and launch methods

#### 9. Read a network in Json Graph

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/9-ReadJsonGraph)

- Create a composable that reads a json graph and creates a new Network

#### 10. Manage the node styles

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/10-AddStyles)

- Make the styles of the nodes dynamic
- Use dynamic components to choose the shape of the nodes

#### 11. Unit and e2e tests

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/7.5-Tests)

- Use vitest for unit tests
- Use cypress for e2e tests

#### 12. Single component Packaging

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/2.5-build_singlecomponent_library)

#### 13. Multiple component Packaging

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/5.5-build_multicomponents_library)

#### 14. Advanced: put complex svg inside nodes

Draw molecules in svg inside nodes with smiles-drawer

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/12-complexNodes?ref_type=heads)

#### 15. Advanced: use slots to make NetworkComponent generic

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/13-Slots?ref_type=heads)

#### 15. Use vueuse useRefhistory composable to make undo and redo

[Exercice and solutions](https://forgemia.inra.fr/codev4graph/codev4graph-trainings/tutoviz/-/tree/14-undo?ref_type=heads)