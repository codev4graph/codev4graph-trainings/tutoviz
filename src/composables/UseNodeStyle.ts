import type { NodeStyleType } from "@/models/NodeStyleType";
import { computed } from "vue";

export function useNodeStyle(props: {nodeStyle: NodeStyleType}) {

	const defaultNodeStyle: NodeStyleType = {
		height: 10,
		width: 20,
		fill: "steelblue",
		strokeWidth: 1,
		stroke: 'lightgrey',
	};

	// Merge defaultStyle and style in props
	const finalStyle = computed(() => {
		return { ...defaultNodeStyle, ...props.nodeStyle };
	});

	return (finalStyle);

}