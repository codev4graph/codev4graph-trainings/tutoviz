import type { NetworkType } from "../models/NetworkType";
import type { LinkType } from "../models/LinkType";
import type { NodeType } from "../models/NodeType";

function getRandomInt(x: number, y: number): number {
	const min = Math.ceil(x);
	const max = Math.floor(y);
	return Math.floor(Math.random() * (max - min + 1)) + min;
  }


export function useRandomNetwork(numNodes: number, numLinks: number, xMax: number, yMax: number): NetworkType {

	const nodes: { [key: string]: NodeType } = {};

	// Generate nodes randomly
	for (let i = 0; i < numNodes; i++) {
		const label = `node${i}`;

		const node: NodeType = {
			id: label,
			label: label,
			x: getRandomInt(0, xMax),
			y: getRandomInt(0, yMax),
		};

		nodes[label] = node;
	}

	const links: Array<LinkType> = [];

	// Generate links randomly
	for (let i = 0; i < numLinks; i++) {
		const sourceNodeId = 'node' + Math.floor(Math.random() * numNodes);
		const targetNodeId = 'node' + Math.floor(Math.random() * numNodes);

		const link: LinkType = {
			id: `Link ${i}`,
			label: `Link ${i}`,
			source: nodes[sourceNodeId],
			target: nodes[targetNodeId]
		};

		links.push(link);
	}

	const network: NetworkType = {
		id: "random",
		nodes: nodes,
		links: links
	};


	return network;
}
