import type { NetworkType } from "../models/NetworkType";
import type { NodeType } from "../models/NodeType";

import { v4 as uuidv4 } from 'uuid';
import type { NetworkStyleType } from "@/models/NetworkStyleType";


export function useReadJsonGraph(jsonGraph: string): { network: NetworkType, networkStyle: NetworkStyleType } {

	const jsonObject = JSON.parse(jsonGraph);

	const network: NetworkType = {
		id: "",
		nodes: {},
		links: []
	};

	const networkStyle: NetworkStyleType = {
		nodeStyles: {}
	}

	if (!jsonObject.graph) {
		throw new Error("graph attribute lacking in json graph format")
	}

	if (!jsonObject.graph.nodes) {
		throw new Error("nodes attribute lacking in json graph format")
	}

	if (!jsonObject.graph.edges) {
		throw new Error("edges attribute lacking in json graph format")
	}

	if (jsonObject.graph.id) {
		network.id = jsonObject.graph.id;
	}
	else {
		network.id = uuidv4();
	}

	if (jsonObject.graph.label) {
		network.label = jsonObject.graph.label;
	}
	else {
		network.label = network.id;
	}


	for (const [id, n] of Object.entries(jsonObject.graph.nodes)) {

		const node: NodeType = {
			id: "",
			x: 0,
			y: 0
		};

		node.id = id;

		if (!n.label) {
			node.label = id;
		}
		else {
			node.label = n.label;
		}

		if (n.metadata && n.metadata.position && n.metadata.position.x) {
			node.x = n.metadata.position.x;
		}


		if (n.metadata && n.metadata.position && n.metadata.position.y) {
			node.y = n.metadata.position.y;
		}


		if (node.id in network.nodes) {
			throw new Error("Duplicated node id : " + node.id);
		}

		if (n.metadata && n.metadata.classes) {
			node.classes = n.metadata.classes;
		}

		if (n.metadata) {
			node.metadata = n.metadata;
		}

		network.nodes[node.id] = node;

	};

	network.links = jsonObject.graph.edges.map((e: { source: string, target: string }) => {

		const source: NodeType = network.nodes[e.source];
		const target: NodeType = network.nodes[e.target];

		return {
			...e,
			source: source,
			target: target
		}

	});

	if (jsonObject.graph.metadata && jsonObject.graph.metadata.style) {
		if (jsonObject.graph.metadata.style.nodeStyles) {
			networkStyle.nodeStyles = jsonObject.graph.metadata.style.nodeStyles;
		}
	}


	return { network, networkStyle };

}