import { forceSimulation, forceLink, forceManyBody, forceCenter } from 'd3-force';
import type { Ref } from 'vue';
import type { NetworkType } from '../models/NetworkType';


export function useForceLayout(network: Ref<NetworkType>,
	width: number, height: number, ended: Ref<boolean>) {

	// Convertir l'objet nodes en tableau
	const nodesArray = Object.values(network.value.nodes);

	console.log({ nodesArray });

	const simulation = forceSimulation(nodesArray);

	simulation.on("end", () => {
		ended.value = true;
	})

	function launch() {
		console.log("launch");
		ended.value = false;
		simulation.force("link", forceLink(network.value.links).id((d: any) => d.id))
			.force("charge", forceManyBody())
			.force("center", forceCenter(width / 2, height / 2))
		}

	function stop() {
		console.log("stop force layout")
		simulation.stop();
		ended.value = true;
	}

	return ({ launch, stop, ended })
}
