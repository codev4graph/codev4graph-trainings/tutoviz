import type { NodeType } from "@/models/NodeType";
import { computed, onMounted, ref, watch } from "vue";
import { drag } from 'd3-drag';
import { select } from 'd3-selection';
import type { Selection, BaseType } from 'd3-selection';
import type { D3DragEvent, DragBehavior } from 'd3-drag';
import { getCurrentInstance } from 'vue';


export function useDragAndDrop(props: { node: NodeType }) {

	const instance = getCurrentInstance();
	if (!instance) {
		throw new Error('useDragAndDrop must be called within a setup function');
	}

	// refs
	const nodeElement = ref<SVGElement>();
	const dragOffsetX = ref(0);
	const dragOffsetY = ref(0);
	const x = ref<number>(0);
	const y = ref<number>(0);

	// Lifecycle hooks 
	onMounted(() => {

		x.value = props.node.x;
		y.value = props.node.y;

		const dragBehavior: DragBehavior<SVGElement, unknown, unknown> = drag<SVGElement, unknown>()
			.on('drag', (event: D3DragEvent<SVGElement, unknown, unknown>) => {
				onDrag(event);
			})
			.on('start', (event: D3DragEvent<SVGElement, unknown, unknown>) => {
				onDragStart(event);
			})
			.on('end', (event: D3DragEvent<SVGElement, unknown, unknown>) => {
				onDragEnd();
			})

		if (nodeElement.value) {
			const selection: Selection<SVGElement, unknown, BaseType, unknown> = select(nodeElement.value);
			dragBehavior(selection);
		}

	});

	// computed

	// To change the cursor when hovering a node
	const cursor = computed(() => {
		return `cursor: ${dragOffsetX.value ? 'grabbing' : 'grab'}`
	});

	// watchers

	watch(() => props.node.x, (newX) => {
		x.value = newX;
		dragOffsetX.value = 0;
	});

	watch(() => props.node.y, (newY) => {
		y.value = newY;
		dragOffsetY.value = 0;
	});

	// Methods

	const onDragStart = (event: D3DragEvent<SVGElement, unknown, unknown>) => {
		dragOffsetX.value = event.x - x.value;
		dragOffsetY.value = event.y - y.value;
		instance.emit('dragstart');
	}

	const onDragEnd = () => {
		dragOffsetX.value = dragOffsetY.value = 0;
		instance.emit('dragend');
	}

	const onDrag = (event: D3DragEvent<SVGElement, unknown, unknown>) => {
		x.value = event.x - dragOffsetX.value;
		y.value = event.y - dragOffsetY.value;
		instance.emit('drag', props.node.id, x.value, y.value);
	}

	return {
		cursor,
		nodeElement
	};

}