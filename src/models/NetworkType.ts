import type { LinkType } from "./LinkType";
import type { NodeType } from "./NodeType";

export interface NetworkType {
	id: string;
	label?: string;
	nodes: { [key: string]: NodeType },
	links: Array<LinkType>
}
