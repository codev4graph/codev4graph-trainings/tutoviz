import type { NodeStyleType } from "./NodeStyleType";

export interface NetworkStyleType {
	nodeStyles: { [key: string]: NodeStyleType },
}