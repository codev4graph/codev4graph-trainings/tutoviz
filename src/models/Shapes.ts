enum Shapes {
	rect = "rect", 
	circle = "circle", 
	molecule = "molecule"
}

export { Shapes };