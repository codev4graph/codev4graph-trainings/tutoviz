import type { NodeType } from "./NodeType";

export interface LinkType {
	id: string;
	label?: string;
	source: NodeType;
	target: NodeType;
}
