export interface NodeType {
	id: string;
	label?: string;
	x: number;
	y: number;
	classes?: Array<string>;
	metadata?: { [key: string]: string };
}
