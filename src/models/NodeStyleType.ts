import type { Shapes } from "./Shapes";

export interface NodeStyleType {
  height?: number;
  width?: number;
  fill?: string;
  strokeWidth?: number;
  stroke?: string; 
  displayLabel?: boolean;
  shape?: Shapes;
}
