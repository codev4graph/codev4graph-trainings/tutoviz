import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vitest/config";
import vue from "@vitejs/plugin-vue";

import path from "path";

// https://github.com/vuetifyjs/vuetify-loader/tree/next/packages/vite-plugin
import vuetify from "vite-plugin-vuetify";

// https://vitejs.dev/config/
export default defineConfig({
	base: './',
	plugins: [vue(), vuetify({ autoImport: true })],

	resolve: {
		alias: {
			"@": fileURLToPath(new URL("./src", import.meta.url)),
		},
	},
	test: {
		coverage: {
			provider: "c8",
			reporter: ["text", "json", "html"],
		},
		globals: true,
		environment: "jsdom",
		deps: {
			inline: ["vuetify"],
		},
	},
});
